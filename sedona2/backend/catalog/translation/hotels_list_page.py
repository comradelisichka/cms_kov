from modeltranslation.translator import TranslationOptions, register
from ..models import HotelsListPage


@register(HotelsListPage)
class HotelsListPageTranslationOptions(TranslationOptions):
    pass

from django.urls import path
from .views import HotelFilterView

urlpatterns = []
urlpatterns += [path('filter/', HotelFilterView.as_view(), name='filter')]
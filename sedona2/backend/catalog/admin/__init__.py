from .hotels_list_page import HotelsListPageAdmin  # noqa
from .hotels import HotelAdmin # noqa
from .type_of_stay import HotelTypeAdmin # noqa
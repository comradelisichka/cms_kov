from ..models.hotels import Hotel
from django.contrib import admin



@admin.register(Hotel)
class HotelAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'price', 'rating')
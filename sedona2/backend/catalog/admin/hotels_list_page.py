from ..models.hotels_list_page import HotelsListPage
from django.contrib import admin
from garpix_page.admin import BasePageAdmin


@admin.register(HotelsListPage)
class HotelsListPageAdmin(BasePageAdmin):
    pass

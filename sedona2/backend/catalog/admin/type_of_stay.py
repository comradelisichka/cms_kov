from django.contrib import admin
from ..models.type_of_stay import HotelType

@admin.register(HotelType)
class HotelTypeAdmin(admin.ModelAdmin):
    list_display = ('name',)
    ordering = ('id',)


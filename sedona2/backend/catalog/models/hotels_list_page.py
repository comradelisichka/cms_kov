from django.db import models
from garpix_page.models import BaseListPage
from garpix_utils.paginator import GarpixPaginator

from ..models.hotels import Hotel


class HotelsListPage(BaseListPage):
    paginate_by = 3
    template = 'pages/hotels_list.html'

    def get_context(self, request=None, *args, **kwargs):
        from ..serializers import HotelSerializer
        from ..models.type_of_stay import HotelType
        context = super().get_context(request, *args, **kwargs)

        object_list = Hotel.objects.all()
        paginator = GarpixPaginator(object_list, self.paginate_by)
        try:
            page = int(request.GET.get('page', 1))
        except ValueError:
            page = 1

        paginated_object_list = paginator.get_page(page)

        hotels = {'hotels': HotelSerializer(paginated_object_list, many=True).data}
        context.update(hotels)

        types = {'types': HotelType.objects.values()}
        context.update(types)

        return context

    class Meta:
        verbose_name = "Каталог гостиниц"
        verbose_name_plural = "Каталог гостиниц"
        ordering = ('-created_at',)

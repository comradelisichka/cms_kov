from django.db import models

class HotelType(models.Model):

    name = models.CharField(verbose_name='тип жилья', max_length=30, default='Гостиница', blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Тип жилья'
        verbose_name_plural = 'Типы жилья'
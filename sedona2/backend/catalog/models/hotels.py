from django.db import models
from garpix_notify.utils import get_file_path
from ..models.type_of_stay import HotelType

class Hotel(models.Model):

    img = models.ImageField(verbose_name='Фото', blank=True, null=True, upload_to=get_file_path)
    name = models.CharField(verbose_name='название жилья', max_length=60, blank=True)
    type = models.ForeignKey(HotelType, verbose_name='тип жилья', related_name='hotels', null=True, blank=True,
                             on_delete=models.SET_NULL)
    price = models.IntegerField(verbose_name='минимальная цена за ночь', blank=True)
    rating = models.DecimalField(verbose_name='рейтинг', max_digits=2, decimal_places=1)

    pool = models.BooleanField(verbose_name='бассейн', blank=True, null=True)
    parking_lot = models.BooleanField(verbose_name='парковка', blank=True, null=True)
    wifi = models.BooleanField(verbose_name='Wi-Fi', blank=True, null=True)

    def __str__(self):
        return self.name

    def get_serializer(self):
        from ..serializers import HotelSerializer
        return HotelSerializer

    class Meta:
        verbose_name = 'Гостиница'
        verbose_name_plural = 'Гостиницы'

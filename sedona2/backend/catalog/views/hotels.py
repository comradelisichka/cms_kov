from ..models import Hotel
from django.views.generic import ListView
from django.db.models import Q




class HotelFilterView(ListView):
    # для формы фильтрации в шаблоне

    model = Hotel
    template_name = 'pages/hotels.html'
    paginate_by = 3

    def get_context_data(self, *, object_list=None, **kwargs):
        from ..serializers import HotelSerializer
        from ..models.type_of_stay import HotelType

        context = super().get_context_data(**kwargs)

        hotels = {'hotels': Hotel.objects.all()}
        context.update(hotels)

        types = {'types': HotelType.objects.values()}
        context.update(types)

        return context

    def get_queryset(self):
        queryset = Hotel.objects.filter(Q(pool__in=self.request.GET.getlist("pool")) |
        Q(parking_lot__in=self.request.GET.getlist("parking_lot")) |
        Q(wifi__in=self.request.GET.getlist("parking_lot")) |
        Q(type__in=self.request.GET.getlist("type")))

        return queryset
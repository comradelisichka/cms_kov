from rest_framework import serializers
from ..models import Hotel


class HotelSerializer(serializers.ModelSerializer):
    type = serializers.CharField(source='type.name', read_only=True)
    class Meta:
        model = Hotel
        fields = '__all__'
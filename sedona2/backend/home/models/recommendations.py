from django.db import models

from garpix_utils.file import get_file_path

class Recommendation(models.Model):
    #icon = models.ImageField(null=True, verbose_name='значок рекомендации', upload_to=get_file_path)
    title = models.CharField(max_length=30, unique=True, null=True, verbose_name='имя рекомендации')
    description = models.CharField(max_length=80, unique=True, null=True, verbose_name='пояснение рекомендации')

    class Meta:
        verbose_name = 'Рекомендация'
        verbose_name_plural = 'Рекомендации'
        ordering = ['id']
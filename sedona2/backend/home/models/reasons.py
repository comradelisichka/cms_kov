from django.db import models

class Reason(models.Model):
    title = models.CharField(max_length=50, unique=True, null=True, verbose_name='имя причины')
    number = models.CharField(max_length=4, null=True, verbose_name='номер причины')
    description = models.CharField(max_length=80, unique=True, null=True, verbose_name='пояснение причины')


    class Meta:
        verbose_name = 'Причина'
        verbose_name_plural = 'Причины'
        ordering = ['number']
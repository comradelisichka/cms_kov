from django.db import models
from garpix_page.models import BasePage
#from garpix_notify.models import Notify
#from django.conf import settings
from .reasons import Reason
from .recommendations import Recommendation


class HomePage(BasePage):
    template = "pages/home.html"

    title1 = models.CharField(max_length=80, verbose_name='первый заголовок', null=True, default='Welcome')
    description1 = models.CharField(max_length=100, verbose_name='первое описание', null=True,
                                    default='to the gorgeous')

    title2 = models.CharField(max_length=80, verbose_name='второй заголовок', null=True, default='Sedona')
    description2 = models.CharField(max_length=100, verbose_name='второе описание', null=True,
                                    default='"Because the Grand Canyon sucks!"')

    title3 = models.CharField(max_length=80, verbose_name='третий заголовок', null=True,
                              default='Седона - небольшой городок в Аризоне, заслуживающий большего!')
    description3 = models.CharField(max_length=100, verbose_name='третье описание', null=True,
                                    default='Рассмотрим 5 причин, по которым Седона лучше, чем Гранд Каньон!')

    title4 = models.CharField(max_length=80, verbose_name='четвёртый заголовок', null=True,
                              default='Заинтересовались?')

    description4 = models.CharField(max_length=100, verbose_name='четвёртое описание', null=True,
                                    default='Укажите предполагаемые даты поездки и забронируйте гостиницу в Седоне')

    reasons = models.ManyToManyField(Reason, verbose_name='Причины', related_name='reasons_on_index_page')
    recommendations = models.ManyToManyField(Recommendation, verbose_name='Рекомендации',
                                             related_name='recommendations_on_index_page')

    def get_context(self, request=None, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        reasons = {'all_reasons': Reason.objects.values()}
        recommendations = {'all_recs': Recommendation.objects.values()}
        context.update(reasons)
        context.update(recommendations)
        return context

    class Meta:
        verbose_name = "Главная"
        verbose_name_plural = "Главная"
        ordering = ("-created_at",)

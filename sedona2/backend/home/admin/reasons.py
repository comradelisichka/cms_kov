from ..models.reasons import Reason
from django.contrib import admin



@admin.register(Reason)
class ReasonAdmin(admin.ModelAdmin):
    list_display = ('title', 'number', 'description',)
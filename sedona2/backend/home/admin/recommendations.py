from ..models.recommendations import Recommendation
from django.contrib import admin


@admin.register(Recommendation)
class RecommendationAdmin(admin.ModelAdmin):
    list_display = ('title', 'description',)